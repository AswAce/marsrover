package com.asen.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for (int i = 0; i < t; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            List<Integer> number = new ArrayList<>();
            number.add(a + 1 * b);
            for (int j = 1; j < n; j++) {
                number.add((int) (number.get(j - 1) + Math.pow(2, j) * b));
            }
            for (Integer integer : number) {
                System.out.print(integer+" ");
            }
            System.out.println();

        }
        in.close();

    }
}
