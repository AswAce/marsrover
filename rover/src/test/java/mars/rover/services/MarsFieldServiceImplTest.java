package mars.rover.services;

import mars.rover.entities.MarsField;
import mars.rover.entities.Rover;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class MarsFieldServiceImplTest {

    private MarsFieldService marsFieldService;
    private RoverService roverService;

    @BeforeEach
    public void init() {
        roverService = new RoverServiceImpl();
        marsFieldService = new MarsFieldServiceImpl(roverService);
    }

    @Test
    public void enterFieldSizeTest() {
        MarsField marsField = marsFieldService.enterFieldSize("22");

        Assertions.assertEquals(marsField.getColumn(), 2);
        Assertions.assertEquals(marsField.getRow(), 2);

    }

    @Test
    public void PrivateMethodFlipFieldTest() {
        MarsField marsField = marsFieldService.enterFieldSize("22");
        String[][] field = marsField.getField();

        String testSuere = field[1][0];
        Assertions.assertEquals(testSuere, "00");
    }

    @Test
    public void testInputMarsFieldTestTrue() {


        Assertions.assertTrue(marsFieldService.testMarsFieldInputField("55"));

    }

    @Test
    public void testInputMarsFieldTestFalse() {
        Assertions.assertFalse(marsFieldService.testMarsFieldInputField("10"));
        Assertions.assertFalse(marsFieldService.testMarsFieldInputField("00"));
        Assertions.assertFalse(marsFieldService.testMarsFieldInputField("01"));
        Assertions.assertFalse(marsFieldService.testMarsFieldInputField("AA"));

    }

    @Test
    public void MoveRoverLeftInputTest() {


    }

    @Test
    public void StartOperation() {
        MarsField marsFieldTest = marsFieldService.enterFieldSize("55");
        ArrayList<Rover> testRovers = roverService.ReceiveRoverCoordinates("12N LMLMLMLMM 33E MMRMMRMRRM");
        marsFieldTest.setRovers(testRovers);
        marsFieldService.startOperation(marsFieldTest);
        String[][] fieldWIthLendedRovers = marsFieldTest.getField();

        Rover testRoverOne = marsFieldTest.getRovers().get(0);
        Rover testRoverTwo = marsFieldTest.getRovers().get(1);


        Assertions.assertEquals(fieldWIthLendedRovers[2][1],"1R");
        Assertions.assertEquals(fieldWIthLendedRovers[4][4],"2R");

        Assertions.assertEquals(testRoverOne.getX(),2);
        Assertions.assertEquals(testRoverOne.getY(),3);
        Assertions.assertEquals(testRoverOne.getLookingDirection(),"N");

        Assertions.assertEquals(testRoverTwo.getX(),5);
        Assertions.assertEquals(testRoverTwo.getY(),1);
        Assertions.assertEquals(testRoverTwo.getLookingDirection(),"E");



    }
}
