package mars.rover.services;

import mars.rover.entities.MarsField;
import mars.rover.entities.Rover;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
public class RoverServiceImplTest {

    private RoverService roverService;
    private MarsFieldService marsField;

    @BeforeEach
    public void init() {
        roverService = new RoverServiceImpl();

        marsField = new MarsFieldServiceImpl(roverService);
    }

    @Test
    public void transferNumberToDirectionTestAllValid() {
        int northNumberNumber = 1;
        int eastNumber = 2;
        int southNumber = 3;
        int westNumber = 4;

        Assertions.assertEquals("N", roverService.transferNumberToDirection(northNumberNumber));
        Assertions.assertEquals("E", roverService.transferNumberToDirection(eastNumber));
        Assertions.assertEquals("S", roverService.transferNumberToDirection(southNumber));
        Assertions.assertEquals("W", roverService.transferNumberToDirection(westNumber));
    }

    @Test
    public void transferNumberToDirectionTestErrorNumber() {
        Assertions.assertNull(roverService.transferNumberToDirection(100));
    }

    @Test
    public void changeDirectionRightTest() {
        Rover testRover = new Rover();
        testRover.setX(1);
        testRover.setX(1);
        testRover.setLookingDirection("E");
        roverService.changeDirection(1, testRover);
        Assertions.assertEquals("S", testRover.getLookingDirection());


    }

    @Test
    public void changeDirectionLeftTest() {
        Rover testRover = new Rover();
        testRover.setX(1);
        testRover.setX(1);
        testRover.setLookingDirection("S");
        roverService.changeDirection(-1, testRover);
        Assertions.assertEquals("E", testRover.getLookingDirection());

    }

    @Test
    public void receiveRoverCoordinatesTest() {
        ArrayList<Rover> testRovers = roverService.ReceiveRoverCoordinates("12N LMLMLMLMM 33E MMRMMRMRRM");

        Rover testRoverOne = testRovers.get(0);
        Rover testRoverTwo = testRovers.get(1);
        Assertions.assertEquals(testRoverOne.getLookingDirection(), "N");
        Assertions.assertEquals(testRoverOne.getX(), 1);
        Assertions.assertEquals(testRoverOne.getY(), 2);
        Assertions.assertEquals(testRoverOne.getMovements(), "LMLMLMLMM");

        Assertions.assertEquals(testRoverTwo.getLookingDirection(), "E");
        Assertions.assertEquals(testRoverTwo.getX(), 3);
        Assertions.assertEquals(testRoverTwo.getY(), 3);
        Assertions.assertEquals(testRoverTwo.getMovements(), "MMRMMRMRRM");

    }

    @Test
    public void driveRoverTestMoveEast() {
        Rover testRover = new Rover();
        testRover.setX(1);
        testRover.setY(1);
        testRover.setLookingDirection("E");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(2, testRover.getX());
        Assertions.assertEquals(1, testRover.getY());
        Assertions.assertEquals("E", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveWest() {
        Rover testRover = new Rover();
        testRover.setX(2);
        testRover.setY(1);
        testRover.setLookingDirection("W");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(1, testRover.getX());
        Assertions.assertEquals(1, testRover.getY());
        Assertions.assertEquals("W", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveNorth() {
        Rover testRover = new Rover();
        testRover.setX(2);
        testRover.setY(1);
        testRover.setLookingDirection("N");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(2, testRover.getX());
        Assertions.assertEquals(2, testRover.getY());
        Assertions.assertEquals("N", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveSouth() {
        Rover testRover = new Rover();
        testRover.setX(2);
        testRover.setY(2);
        testRover.setLookingDirection("S");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(2, testRover.getX());
        Assertions.assertEquals(1, testRover.getY());
        Assertions.assertEquals("S", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveOutSideFieldSouth() {
        Rover testRover = new Rover();
        testRover.setX(1);
        testRover.setY(1);
        testRover.setLookingDirection("S");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(1, testRover.getX());
        Assertions.assertEquals(1, testRover.getY());
        Assertions.assertEquals("S", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveOutSideFieldWest() {
        Rover testRover = new Rover();
        testRover.setX(5);
        testRover.setY(5);
        testRover.setLookingDirection("E");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(5, testRover.getX());
        Assertions.assertEquals(5, testRover.getY());
        Assertions.assertEquals("E", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveOutSideFieldNorth() {
        Rover testRover = new Rover();
        testRover.setX(5);
        testRover.setY(5);
        testRover.setLookingDirection("N");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(5, testRover.getX());
        Assertions.assertEquals(5, testRover.getY());
        Assertions.assertEquals("N", testRover.getLookingDirection());
    }

    @Test
    public void driveRoverTestMoveOutSideFieldEast() {
        Rover testRover = new Rover();
        testRover.setX(1);
        testRover.setY(1);
        testRover.setLookingDirection("W");
        testRover.setMovements("M");
        MarsField testField = this.marsField.enterFieldSize("55");

        roverService.driveRover(testField.getField(), testRover);

        Assertions.assertEquals(1, testRover.getX());
        Assertions.assertEquals(1, testRover.getY());
        Assertions.assertEquals("W", testRover.getLookingDirection());
    }


@Test
    public void driveRoverTesRoverNextToUsSouth() {


        MarsField testField = this.marsField.enterFieldSize("55");
        ArrayList<Rover> testRovers = roverService.ReceiveRoverCoordinates("11N L 12S MM");
        testField.setRovers(testRovers);
        this.marsField.startOperation(testField);

        Rover notMovedRoverBecauseSquareIsFull = testField.getRovers().get(1);
Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getX(),1);
Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getY(),2);
Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getLookingDirection(),"S");

    }

    @Test
    public void driveRoverTesRoverNextToUsWest() {


        MarsField testField = this.marsField.enterFieldSize("55");
        ArrayList<Rover> testRovers = roverService.ReceiveRoverCoordinates("11N L 21W MM");
        testField.setRovers(testRovers);
        this.marsField.startOperation(testField);

        Rover notMovedRoverBecauseSquareIsFull = testField.getRovers().get(1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getX(),2);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getY(),1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getLookingDirection(),"W");

    }

    @Test
    public void driveRoverTesRoverNextToUsNorth() {


        MarsField testField = this.marsField.enterFieldSize("55");
        ArrayList<Rover> testRovers = roverService.ReceiveRoverCoordinates("12N L 11N MM");
        testField.setRovers(testRovers);
        this.marsField.startOperation(testField);

        Rover notMovedRoverBecauseSquareIsFull = testField.getRovers().get(1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getX(),1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getY(),1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getLookingDirection(),"N");

    }

    @Test
    public void driveRoverTesRoverNextToUsEast() {


        MarsField testField = this.marsField.enterFieldSize("55");
        ArrayList<Rover> testRovers = roverService.ReceiveRoverCoordinates("21N L 11E MM");
        testField.setRovers(testRovers);
        this.marsField.startOperation(testField);

        Rover notMovedRoverBecauseSquareIsFull = testField.getRovers().get(1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getX(),1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getY(),1);
        Assertions.assertEquals(notMovedRoverBecauseSquareIsFull.getLookingDirection(),"E");

    }
}
