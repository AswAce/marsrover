package mars.rover;

import mars.rover.services.MarsFieldService;
import mars.rover.services.RoverService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Run implements CommandLineRunner {

    private final MarsFieldService marsFieldService;
    private final RoverService roverService;


    public Run(MarsFieldService marsFieldService, RoverService roverService) {

        this.marsFieldService = marsFieldService;
        this.roverService = roverService;

    }

    @Override
    public void run(String... args) throws Exception {



    }




}
