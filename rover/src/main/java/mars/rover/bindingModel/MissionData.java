package mars.rover.bindingModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class MissionData {

    @Size(min = 2, max = 2, message = "Enter valid size for the field")
    private String fieldMap;
    private String roversMovement;
}
