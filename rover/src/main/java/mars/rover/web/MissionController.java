package mars.rover.web;

import mars.rover.bindingModel.MissionData;
import mars.rover.entities.MarsField;
import mars.rover.entities.Rover;
import mars.rover.services.MarsFieldService;
import mars.rover.services.RoverService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller

public class MissionController {

    private final MarsFieldService marsFieldService;
    private final RoverService roverService;


    public MissionController(MarsFieldService marsFieldService, RoverService roverService) {
        this.marsFieldService = marsFieldService;
        this.roverService = roverService;
    }

    @GetMapping("/")
    public String startMission(Model model) {

        if (!model.containsAttribute("missionData")) {
            model.addAttribute("missionData", new MissionData());


        }
        model.addAttribute("rovers");
        return "home";
    }

    @PostMapping("/")
    public String startMissionPost(@Valid @ModelAttribute("missionData") MissionData missionData,
                                   BindingResult bindingResult,
                                   RedirectAttributes redirectAttributes, Model model) {
        boolean b = marsFieldService.testMarsFieldInputField(missionData.getFieldMap());
        if (!b) {
            redirectAttributes.addFlashAttribute("wrongInput", true);
            return "redirect:/";
        }
        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("missionData", missionData);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.missionData", bindingResult);

            return "redirect:/";
        }


        MarsField marsField = marsFieldService.enterFieldSize(missionData.getFieldMap());

        ArrayList<Rover> rovers = roverService.ReceiveRoverCoordinates(missionData.getRoversMovement());
        marsField.setRovers(rovers);
        marsFieldService.startOperation(marsField);
        ArrayList<Rover> rovers1 = marsField.getRovers();
        model.addAttribute("rovers",rovers );

        return "home";
    }
}
