package mars.rover.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rover {
    private int roverNumber;
    private int x;
    private int y;
    private String lookingDirection;
    private String movements;


}
