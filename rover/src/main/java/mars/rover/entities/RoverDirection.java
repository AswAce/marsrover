package mars.rover.entities;

public enum RoverDirection {

    N(1),
    E(2),
    S(3),
    W(4);
    private final int number;


    RoverDirection(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
