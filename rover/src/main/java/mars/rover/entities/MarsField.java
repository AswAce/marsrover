package mars.rover.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarsField {

    private int row;
    private int column;
    private String[][] field;
    private ArrayList<Rover> rovers;


}
