package mars.rover.services;


import mars.rover.entities.Rover;
import mars.rover.entities.RoverDirection;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RoverServiceImpl implements RoverService {

    private static final String LANDING_CODE = "[1-9]{2}[NSEW]{1} [LRM]*";

    @Override
    public ArrayList<Rover> ReceiveRoverCoordinates(String cordinates) {

        ArrayList<Rover> rovers = new ArrayList<>();
        Pattern p = Pattern.compile(LANDING_CODE);   // the pattern to search for
        Matcher m = p.matcher(cordinates);

        while (m.find()) {

            String theGroup = m.group();


            System.out.format(theGroup);
            Rover rover = new Rover();
            rover.setX(Integer.valueOf(String.valueOf(theGroup.charAt(0))));
            rover.setY(Integer.valueOf(String.valueOf(theGroup.charAt(1))));
            rover.setLookingDirection(String.valueOf(theGroup.charAt(2)));
            String[] s = theGroup.split(" ");
            rover.setMovements(s[1]);
            rovers.add(rover);
        }

        return rovers;
    }

    //This method help us to create Array with rovers with their coordinates and movements.
    @Override
    public void changeDirection(int i, Rover rover) {
        String lookingDirection = rover.getLookingDirection();
        RoverDirection roverDirection = RoverDirection.valueOf(lookingDirection);
        int currentNumberDirection = roverDirection.getNumber();
        int newNumber = currentNumberDirection + i;
        if (newNumber == 5) {
            newNumber = 1;
        } else if (newNumber == 0) {
            newNumber = 4;
        }
        String direction = transferNumberToDirection(newNumber);

        rover.setLookingDirection(direction);
        System.out.println();
    }

    // this method is used to flip the rover in different directions depending on the input.
    @Override
    public String transferNumberToDirection(int newNumber) {
        System.out.println();
        switch (newNumber) {
            case 1:
                return "N";
            case 2:
                return "E";
            case 3:
                return "S";
            case 4:
                return "W";
            default:
                return null;
        }

    }

    //We have a number that is equals to every direction, here we transfer it.
    @Override
    public void landRover(String[][] marsLandingField, Rover rover, int roverNumber) {
        String cordinates = "" + (rover.getY() - 1) + (rover.getX() - 1);

        for (int row = 0; row < marsLandingField.length; row++) {
            String[] currentRow = marsLandingField[row];
            for (int col = 0; col < currentRow.length; col++) {
                String curerntCell = marsLandingField[row][col];
                if (curerntCell.equals(cordinates)) {
                    marsLandingField[row][col] = String.valueOf(roverNumber + 1) + "R";
                }
            }
        }


    }

    //Using this method we are landing the rover in last field he is(this can be use for visualization if we want to
// draw a map and put the rover in the final square)
    @Override
    public void driveRover(String[][] marsLandingField, Rover currentRover) {
        boolean checkNextMOveFOrROver = true;
        boolean notGoingOutOfTheField = checkMapFieldSize(marsLandingField, currentRover);
        if (notGoingOutOfTheField) {
            checkNextMOveFOrROver = !checkRoverIfPossibleToMove(marsLandingField, currentRover);
        }


        switch (currentRover.getLookingDirection()) {
            case "N":
                if (notGoingOutOfTheField && checkNextMOveFOrROver

                ) {
                    currentRover.setY(currentRover.getY() + 1);
                }
                break;
            case "E":
                if (notGoingOutOfTheField && checkNextMOveFOrROver

                ) {
                    currentRover.setX(currentRover.getX() + 1);
                }
                break;
            case "S":

                if (notGoingOutOfTheField && checkNextMOveFOrROver
                ) {

                    currentRover.setY(currentRover.getY() - 1);

                }

                break;
            case "W":

                if (notGoingOutOfTheField && checkNextMOveFOrROver

                ) {
                    currentRover.setX(currentRover.getX() - 1);
                }
                break;
        }

    }

    //This is the actual moving depending of current rover direction we are
// checking next square ( if is out of field, if there is another rover)
    //and we do the move
    private boolean checkRoverIfPossibleToMove(String[][] marsLandingField, Rover currentRover) {

        int[] roverCordineates = findRoverCoordinates(marsLandingField, currentRover);
        switch (currentRover.getLookingDirection()) {
            case "N":

                if (marsLandingField[0].length >= currentRover.getY() + 1

                ) {
                    roverCordineates[0] = roverCordineates[0] - 1;
                    String neighborCell = marsLandingField[roverCordineates[0]][roverCordineates[1]];
                    return neighborCell.contains("R");
                }
                return false;


            case "E":


                if (marsLandingField.length >= currentRover.getX() + 1

                ) {
//
                    roverCordineates[1] = roverCordineates[1] + 1;
                    String neighborCell = marsLandingField[roverCordineates[0]][roverCordineates[1]];
                    return neighborCell.contains("R");
                }
                return false;


            case "S":

                if (currentRover.getY() - 1 > 0
                ) {
                    roverCordineates[0] = roverCordineates[0] + 1;
                    String neighborCell = marsLandingField[roverCordineates[0]][roverCordineates[1]];
                    return neighborCell.contains("R");
                }
                return false;
            default:

                if (currentRover.getX() - 1 > 0

                ) {
                    roverCordineates[1] = roverCordineates[1] - 1;
                    String neighborCell = marsLandingField[roverCordineates[0]][roverCordineates[1]];
                    return neighborCell.contains("R");
                }
                return false;
        }


    }

    //this method check the map if there is rover on the next move
    private boolean checkMapFieldSize(String[][] marsLandingField, Rover currentRover) {
        switch (currentRover.getLookingDirection()) {
            case "N":

                if (marsLandingField[0].length >= currentRover.getY() + 1

                ) {

//                    currentRover.setY(currentRover.getY() + 1);
                    return true;
                }
                return false;

            case "E":


                if (marsLandingField.length >= currentRover.getX() + 1

                ) {
//                    currentRover.setX(currentRover.getX() + 1);
                    return true;

                }
                return false;

            case "S":

                if (currentRover.getY() - 1 > 0
                ) {
//                    currentRover.setY(currentRover.getY() - 1);
                    return true;
                }
                return false;

            default:

                if (currentRover.getX() - 1 > 0

                ) {
//                    currentRover.setX(currentRover.getX() - 1);
                    return true;
                }
                return false;

        }


    }
//this method check the map if with the next move the rover will go out of the field

    private int[] findRoverCoordinates(String[][] marsLandingField, Rover currentRover) {
        for (int i = 0; i < marsLandingField.length; i++) {
            String[] currentR0w = marsLandingField[i];
            for (int j = 0; j < currentR0w.length; j++) {
                String s = marsLandingField[i][j];
                if (s.contains(currentRover.getY() - 1 + "" + (currentRover.getX() - 1))) {

                    return new int[]{i, j};
                }
            }
        }
        return new int[]{11, 11};
    }
//when we need to find rover current coordinate (to check next mover)
}
