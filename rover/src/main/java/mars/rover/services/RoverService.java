package mars.rover.services;

import mars.rover.entities.Rover;

import java.util.ArrayList;

public interface RoverService {

    ArrayList<Rover> ReceiveRoverCoordinates(String coordinates);

    void changeDirection(int i, Rover rover);

    String transferNumberToDirection(int newNumber);

    void landRover(String[][] marsLandingField, Rover rover, int roverNumber);

    void driveRover(String[][] marsLandingField, Rover currentRover);
}
