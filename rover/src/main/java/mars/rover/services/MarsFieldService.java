package mars.rover.services;

import mars.rover.entities.MarsField;

public interface MarsFieldService {

    void startOperation(MarsField marsField);

    MarsField enterFieldSize(String data );

    boolean testMarsFieldInputField(String input);

    String correctMarsField(String s);
}
