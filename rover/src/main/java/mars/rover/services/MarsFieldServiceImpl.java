package mars.rover.services;

import mars.rover.entities.MarsField;
import mars.rover.entities.Rover;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MarsFieldServiceImpl implements MarsFieldService {
    @Autowired
    private final RoverService roverService;


    public MarsFieldServiceImpl(RoverService rootService) {
        this.roverService = rootService;
    }

    @Override
    public void startOperation(MarsField marsField) {
        ArrayList<Rover> rovers = marsField.getRovers();
        for (int rover = 0; rover < rovers.size(); rover++) {
            rovers.get(rover).setRoverNumber(rover + 1);
            String[][] marsLandingField = marsField.getField();

            moveRover(marsLandingField, rovers.get(rover));
            this.roverService.landRover(marsLandingField, rovers.get(rover), rover);
        }
//this is the main method to start the workflow


        for (Rover rover : rovers) {

        }
        ;
        printMa3x(marsField.getField());

    }

    private void moveRover(String[][] marsLandingField, Rover currentRover) {

        char[] chars = currentRover.getMovements().toCharArray();
        for (char command : chars) {
            switch (command) {
                case 'M':
                    this.roverService.driveRover(marsLandingField, currentRover);
                    break;
                case 'L':

                    this.roverService.changeDirection(-1, currentRover);
                    break;
                case 'R':
                    this.roverService.changeDirection(1, currentRover);

                    break;
            }
        }


    }


    private void printMa3x(String[][] ma3x) {
        System.out.println();
        for (int row = 0; row < ma3x.length; row++) {
            for (int col = 0; col < ma3x[row].length; col++) {
                System.out.print(ma3x[row][col] + " ");
            }
            System.out.println();
        }
    }

    //printing the ma3x
    @Override
    public MarsField enterFieldSize(String size) {


        MarsField marsField = new MarsField();
        int rows = Integer.parseInt(String.valueOf(size.charAt(0)));
        int columns = Integer.parseInt(String.valueOf(size.charAt(1)));

        marsField.setRow(rows);
        marsField.setColumn(columns);
        String[][] field = new String[rows][columns];
        for (int r = 0; r < rows; r++) {
            for (int col = 0; col < columns; col++) {
                field[r][col] = (r) + "" + (col);

            }
        }

        field = flipField(field);
        marsField.setField(field);
        return marsField;
    }

    //This method create our 2 dimensional array.
    @Override
    public boolean testMarsFieldInputField(String input) {
        for (char n : input.toCharArray()) {
            if (!Character.isDigit(n)) {
                return false;
            } else if (n == '0') {
                return false;
            }
        }
        return true;
    }
//Here we are testing if the field input is only numbers between 1-9

    @Override
    public String correctMarsField(String s) {

        while (!testMarsFieldInputField(s) || s.length() > 2) {
            System.out.println("Field size can be only be Two numbers between One and Nine");

        }
        return s;
    }
    //checking the size of the field;

    private String[][] flipField(String[][] field) {
        for (int r = 0; r < field.length / 2; r++) {
            String[] startRows = field[r];
            String[] lastRows = field[field.length - 1 - r];
            int newRow = field.length - 1 - r;
            field[r] = lastRows;
            field[newRow] = startRows;
        }
        return field;
    }

    //Because we have a condition where Cell value 00 is at the bottom left we need to flip the Ma3x
}
